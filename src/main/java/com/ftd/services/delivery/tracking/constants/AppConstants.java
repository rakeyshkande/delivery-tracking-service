package com.ftd.services.delivery.tracking.constants;

/**
 * @author Rehman
 *
 */

public final class AppConstants {

    private AppConstants() {
    }

    /*----------------Headers----------------*/
    public static final String LOCALE_HEADER = "Accept-Language";

    /*----------Execption Messages----------*/
    public static final String VALIDATION = "validation";
    public static final String UNMODIFIED_ENTITY_EXCEPTION = "UnModifiedEntityException";
    public static final String RULES_VALIDATION_EXCEPTION = "RulesValidationException";
    public static final String DELETE_TEMPLATE = "DELETE_TEMPLATE";
    public static final String UPDATE_TEMPLATE = "UPDATE_TEMPLATE";


    /*----------Application Constants-------*/
    public static final String BASE_PATH_MESSAGES = "messages/message";

    /*---------DROOLS Constants------------*/
    public static final String OPERATOR = "operator";
    public static final String DEFAULT = "default";
    public static final String CONDITION_JSON = "conditionJson";
    public static final String ACTION_JSON = "actionJson";

    public static final String ENTITY_PLACE_HOLDER = "${entity}";
    public static final String ENTITY = "entity";
    public static final String FIELD_PLACE_HOLDER = "${field}";
    public static final String FILED = "field";
    public static final String VALUE_PLACE_HOLDER = "${value}";
    public static final String VALUE = "value";
    public static final String ACTION_TYPE_PLACE_HOLDER = "${actionType}";
    public static final String ACTION_TYPE = "actionType";
    public static final String TEMPLATE_PLACE_HOLDER = "${template}";

    public static final String TEMPLATE = "template";
    public static final String ORDER_CONFIRM = "ORDER_CONFIRM";
    public static final String EMAIL = "EMAIL";
    public static final String ORDER_TEMPLATE = "order-confirm";
    public static final String DEFAULT_SOURCE_CODE = "552";
    public static final String EVENT = "event";
    public static final String SUBJECT = "Subject";

}
