package com.ftd.services.delivery.tracking.pubsub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.delivery.tracking.config.Pubsubconfig;
import com.ftd.services.delivery.tracking.entity.MemberCarrier;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GlobalSchedulerPublisher {

    @Autowired
    private PublisherUtil publisherUtil;

    @Autowired
    private Pubsubconfig pubsubconfig;

    public void publish(MemberCarrier message) {
        Gson gson = new Gson();
        String msgData = gson.toJson(message);
        String globalSchedulerTopic = pubsubconfig.getPubSub().getGlobalSchedulerTopic();
        publisherUtil.publish(msgData, globalSchedulerTopic);
        log.info("Event has been published with message {} to {} topic.", msgData, message);
    }

}
