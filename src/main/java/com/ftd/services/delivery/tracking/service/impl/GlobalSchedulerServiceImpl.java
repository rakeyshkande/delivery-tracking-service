package com.ftd.services.delivery.tracking.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftd.services.delivery.tracking.entity.MemberCarrier;
import com.ftd.services.delivery.tracking.pubsub.GlobalSchedulerPublisher;
import com.ftd.services.delivery.tracking.service.GlobalSchedulerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GlobalSchedulerServiceImpl implements GlobalSchedulerService {

    @Autowired
    GlobalSchedulerPublisher globalPublisher;

    @Override
    public void process(String startMsg, Map<String, String> localHeaders, String message) {
        // TODO Auto-generated method stub
        log.info("subbu - {}", message);
    }

    @Override
    public void startScheduler() {
        // TODO Auto-generated method stub
        log.info("subbu - in start scheduler");
        MemberCarrier memCar = new MemberCarrier();
        memCar.setMemberNo("2080A");
        memCar.setCarrierId("1");
        globalPublisher.publish(memCar);
        log.info("Successfully published the event");
    }

}
