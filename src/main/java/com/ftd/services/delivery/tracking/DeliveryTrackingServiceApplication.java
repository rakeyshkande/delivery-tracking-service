package com.ftd.services.delivery.tracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication(scanBasePackages= {"com.ftd"})
public class DeliveryTrackingServiceApplication {

	public static void main(String[] args) {
	    log.info("Starting Spring boot application - Delivery Tracking Microservices");
	    SpringApplication.run(DeliveryTrackingServiceApplication.class, args);
	}

}
