package com.ftd.services.delivery.tracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftd.services.delivery.tracking.service.GlobalSchedulerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
//@RefreshScope
@RequestMapping(value = "delivery-tracking", produces = "application/hal+json")
public class GlobalSchedulerController {

    @Autowired
    GlobalSchedulerService globalSchedulerService;
    
    @GetMapping(value = "/{siteId}/startScheduler", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> startScheduler(
            @Validated @PathVariable(name = "siteId", required = true) String siteId) {
        log.info("Invoked startScheduler");
        globalSchedulerService.startScheduler();
        return ResponseEntity.ok("SUCCESS");
    }
}
