package com.ftd.services.delivery.tracking.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MemberCarrier {

    private String memberNo;
    private String carrierId;
    @Override
    public String toString() {
        return "MemberCarrier [memberNo=" + memberNo + ", carrierId=" + carrierId + "]";
    }
    
}
