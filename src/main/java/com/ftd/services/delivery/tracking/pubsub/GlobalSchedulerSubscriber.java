package com.ftd.services.delivery.tracking.pubsub;

import org.springframework.stereotype.Component;

import com.ftd.commons.pubsub.util.AbstractSubscriber;
import com.google.pubsub.v1.PubsubMessage;

import lombok.extern.slf4j.Slf4j;

@Component("globalSchedulerSubscriber")
@Slf4j
public class GlobalSchedulerSubscriber extends AbstractSubscriber {

    @Override
    public Status onSubscribe(PubsubMessage message) {
        String data = message.getData().toStringUtf8();
        log.info("Global Scheduler Event Subscriber -> Message recieved with data: {}", data);

        return null;
    }

}
